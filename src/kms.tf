resource "aws_kms_key" "default" {
  description             = "default-kms-${var.env}"
  deletion_window_in_days = 7
  tags = {
    Name = "default-kms-${var.env}"
  }
}

resource "aws_kms_alias" "default" {
  name          = "alias/default-kms-${var.env}"
  target_key_id = aws_kms_key.default.key_id
}