terraform {
  required_version = ">=1.2.3"
  backend "s3" {
    region         = "ap-southeast-2"
  }
  required_providers {
    aws = {
      version = "~> 4.21.0"
    }
  }
}